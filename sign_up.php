<?php include('lib/header_home.php'); ?>
<?php

// Include config file
require 'database.php';

if(isset($_POST['submit']))
    {
    	$query = "SELECT * FROM tbl_user WHERE email = :email";  
                $statement = $conn->prepare($query);  
                $statement->execute(  
                     array(  
                          'email'     =>     $_POST["email"]  
                     )  
                );  
                $count = $statement->rowCount();

    	$name= $_POST['username'];
        $email= $_POST['email'];
        $password=$_POST['password'];
        $confirmpassword = $_POST['confirmpassword'];

		if ($name == "" OR $email == "" OR $password == "" OR $confirmpassword == "") {
			$errMsg = "Field must not be Empty";
		}

		else if (strlen($name) < 3) {
			$errMsg = "Username is too Short!";
		}

        else if($count == 1) {
        	$errMsg = "Email already exists!";
        }  

		else if (strlen($password) < 3) {
			$errMsg = "Password is too Short!";
		}

		else if($password != $confirmpassword) {
                $errMsg = "Password not Matched!";
		}

		elseif(preg_match('/[^a-z0-9_-]+/i', $username)){
			$errMsg = "Username must only contain alphanumerical, dashes, and underscores!";
		}

      else  
           {

             $sql = 'INSERT INTO tbl_user(name,email,password) VALUES(:username,:email,:password)';

             $stat = $conn->prepare($sql);

             $stat->execute([':username'=> $name, ':email'=>$email, ':password' => $password]);

              header("location:Login.php");
            }
        }

?>



 <?php
				if(isset($errMsg)){
					echo '<div style="color:#FF0000;text-align:center;font-size:17px;">'.$errMsg.'</div>';
				}
			?> 

<div class="modal-dialog text-center">
		<div class="col-sm-10 main-section">
			<div class="modal-content">
				
				<div class="col-12 user-img">
					<img src="img/boss.png">	
				</div>

				<form class="col-12" method="post">
					<div class="form-group">
						<input type="text"  name="username" class="form-control" placeholder="Username">
					</div>
					<div class="form-group">
						<input type="email" name="email"class="form-control" placeholder="Email">
					</div>
					<div class="form-group">
						<input type="password" name="password" class="form-control" placeholder="Password">
					</div>
					<div class="form-group">
						<input type="password" name="confirmpassword" class="form-control" placeholder="Retype-Password">
					</div>
					<label class="">I agree to the terms and agreement
						<input type="checkbox"  >
						<!-- <span class="checkmark"></span> -->
					</label>
					<button type="submit" name="submit" class="btn"><i class="fas fa-sign-in-alt" value="Submit"></i>Sign Up</button>

				</form>

				<div class="col-12 forgot">
	
				</div>
			</div> <!--  End of Modal Content -->
		</div>
	</div>

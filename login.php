<?php

session_start();

require 'database.php';

   if(isset($_POST["login"]))  
      {  
           if(empty($_POST["username"]) || empty($_POST["password"]))  
           {  
                $errMsg = 'All fields are required';  
           }  
           else  
           {  
                $query = "SELECT * FROM tbl_user WHERE name = :username AND password = :password";  
                $statement = $conn->prepare($query);  
                $statement->execute(  
                     array(  
                          'username'     =>     $_POST["username"],  
                          'password'     =>     $_POST["password"]  
                     )  
                );  
                $count = $statement->rowCount();  
                $fetch = $statement->fetch(PDO::FETCH_OBJ);  

                if($count > 0)  
                {  
                  if($_POST['username'] && $_POST['password'] == 'admin') {
                     $_SESSION["id"] =  $fetch->user_id;
                     $_SESSION["username"] = $_POST["username"];   
                     header("location:admin/admin_dashboard.php");

               
                   }
                   else{
                     $_SESSION["id"] =  $fetch->user_id;
                     $_SESSION["username"] = $_POST["username"];   
                     header("location:client/client_dashboard.php");
                  }
                }  
                else  
                {  
                     $errMsg = 'Wrong Data';  
                } 

           }  
      }  
 
 ?>  
 

<?php include('lib/header_home.php'); ?>

<?php
				if(isset($errMsg)){
					echo '<div style="color:#FF0000;text-align:center;font-size:17px;">'.$errMsg.'</div>';
				}
			?> 

<div class="modal-dialog text-center">
		<div class="col-sm-10 main-section">
			<div class="modal-content">
				
				<div class="col-12 user-img">
					<img src="img/boss.png">	
				</div>
 
				<form class="col-12"  method="post">
					<div class="form-group">
						<input type="text" name="username" class="form-control" placeholder="Enter Username">
					</div>
					<div class="form-group">
						<input type="password" name="password" class="form-control" placeholder="Enter Password">
					</div>

					<button type="submit" name="login" class="btn"><i class="fas fa-sign-in-alt" value="Login"></i>Login</button>

                     <p>Don't have an account? <a href="sign_up.php">Sign Up here</a>.</p>

				</form>

				<div class="col-12 forgot">
<!-- 					<a href="#">Forgot Password?/</a><a href="Sign_up.php">Sign up?</a> -->
				</div>	
			</div> <!--  End of Modal Content -->
		</div>
	</div>
</html>

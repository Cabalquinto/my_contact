 <?php  
 //login_success  
 session_start();  
 
 if(!isset($_SESSION["username"]))  
 {  
      header("location:../index.php");   
 } 
 ?>

 <?php
    include "../database.php";
    $sql = "
        SELECT cmpny.* , clnt.* FROM tbl_company cmpny JOIN tbl_client clnt ON cmpny.company_id = clnt.company_id;
        SELECT * FROM tbl_client;
        ";
            $stat = $conn->prepare($sql);
            $stat->execute();
            $list = $stat->fetchall(PDO::FETCH_OBJ); 
            
        $sql = "SELECT * FROM tbl_user WHERE user_id = ".$_SESSION['id'];
        $stat = $conn->prepare($sql);
        $stat->execute();
        $data = $stat->fetch(PDO::FETCH_OBJ);

?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Client</title>

        <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="../css/style2.css">
        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.min.css">

    </head>
    <body>
  <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Web Application</h3>
            </div>
            <ul class="list-unstyled components" >
                <h3 style="color: yellow; padding-left: 10px;">Client</h3>
                <li class="active">
                </li>
                
                <li>
                    <a href="profile.php">Profile</a>
                </li>
                <li>
                    <a href="contact_view.php">Contact</a>
                </li>
                <li>
                    <a href="company_view.php">Company</a>
                </li>
             </ul>
                <span><img src="../img/Boss-logo.png" width="100%"></span>
        </nav>

        <!-- Page Content Holder -->
        <div id="content">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                  <div class="navbar-header">
                        <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                            <i class="glyphicon glyphicon-align-left"></i>
                            <span>Toggle Sidebar</span>
                        </button>
                    </div>
                    <div class="navbar-header">
                        <button type="button"  class="btn btn-info pull-right">
                            <!-- <i class="glyphicon glyphicon-align-left"></i> -->
                            <a href="../logout.php"><span>Logout</span></a>
                        </button>
                    </div>

                </div>
            </nav> 
                            <div class="card hovercard">
                <div class="cardheader">

                </div>
                <div class="info">
                    <div class="title">
              <center><h1>Welcome <a target="_blank" href=""><?=$data->name;?></a></h1></center> 

                <div class="row">
                    <div class="col-md-12">
                        <span>
                            <img src="../img/loGO.jpg" width="100%";>
                        </span>
                    </div>
                </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>





        <!-- jQuery CDN -->
        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <!-- Bootstrap Js CDN -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- jQuery Custom Scroller CDN -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#sidebar").mCustomScrollbar({
                    theme: "minimal"
                });

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar, #content').toggleClass('active');
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
            });
        </script>
    </body>
</html>

<?php include "../lib/header_client.php";?>
<?php 
    include "../database.php";
    $sql = ("SELECT cmpny. *, clnt.* FROM tbl_company cmpny JOIN tbl_client clnt ON cmpny.company_id = clnt.company_id"); 

            $stat = $conn->prepare($sql);
            $stat->execute();
            $list = $stat->fetchall(PDO::FETCH_OBJ);


         if(isset($_POST['search'])){
            $searrch_name = $_POST['Contact_Name'];
            // $search = "p";

            $sql= "SELECT cmpny.* , clnt.* FROM tbl_company cmpny JOIN tbl_client clnt ON cmpny.company_id = clnt.company_id WHERE client_name LIKE :search";
            // $sql = "SELECT * FROM tbl_client
            $stat = $conn->prepare($sql);
            $stat->execute([':search' => '%'.$searrch_name.'%']);
            $list = $stat->fetchall(PDO::FETCH_OBJ);
            
             // print_r($list);

         }
 ?>
            <center><label><h3>Contacts</h3></label></center>

                <form action="contact_view.php" method="post">
    <div class="row">
        <div class="comp col-md-12">
            <div class="col-md-6"></div>
            <div class="col-md-6"> 
                <input type="text" name="Client_Name" id="Client_Name" placeholder="Client_Name"> 
                <input type="submit" class="btn" name="search" value ="Search"> 
            </div>
        </div>
    </div>
 </form>
                <div class="container-fluid">
                <div class="row">
                <div class="col-md-12">
                <form  method="get" id="<?= $data->company_id; ?>">
                    <table  style="width: 100%; border: 1px solid black;" >
                            <tr>
                                <th>Contact Name</th>
                                <th>Contact Number</th>
                                <th>Company Name</th>
                            </tr>
                        <?php foreach($list as $data): ?>   
                            <tr>
                                <td>
                                    <label for="username" ><?= $data->client_name; ?></a></label> 
                                </td>

                                <td>
                                    <label for="contact_num" ><?= $data->contact_num; ?></label>
                                </td>

                                <td>
                                    <label for="company_name" ><?= $data->company_name; ?></label>
                                </td>

                        </tr>
                        <?php endforeach; ?>
                        <br>  
                    </table>
                </form>
                 </div>
            </div>
        </div>


           </div>
        </div>


        <!-- jQuery CDN -->
        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <!-- Bootstrap Js CDN -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- jQuery Custom Scroller CDN -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#sidebar").mCustomScrollbar({
                    theme: "minimal"
                });

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar, #content').toggleClass('active');
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
            });
        </script>
    </body>
</html>
